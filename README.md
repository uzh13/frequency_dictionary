# frequency_dictionary

Make frequency dictionary for some text

## Getting started

Put you text at this folder at txt format. You may pick filename by single
argument if you run by cli or you can simple put your text in text.txt

Then run

``go run cmd/main.go your_filename``

Or, if you put text in text.txt

```go run cmd/main.go```

Result you may find at ```result.txt```

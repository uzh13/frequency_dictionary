package main

import (
	"brodnikov_bot/internal"
	"brodnikov_bot/pkg"

	"fmt"
	"os"
	"strings"
)

func main() {
	fileName := os.Args[1]
	if fileName == "" {
		fileName = "text.txt"
	}

	source := internal.OpenFile(fileName)
	sourceWords := strings.Fields(source)

	wordsWithFrequency, sortedWords := pkg.Sorting(sourceWords, pkg.ClearWord)

	resultText := ""
	for _, word := range sortedWords {
		count, _ := wordsWithFrequency[word]
		resultText = resultText + fmt.Sprintf("%v %v\n", count, word)
	}

	internal.Save("result.txt", resultText)
}

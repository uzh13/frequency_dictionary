package pkg

import (
	"sort"
)

func Sorting(sourceWords []string, clearWord func(string) string) (dict map[string]int, words []string) {

	dict = map[string]int{}
	words = make([]string, 0, len(sourceWords))

	for _, word := range sourceWords {
		newWord := clearWord(word)

		count, ok := dict[newWord]
		if !ok {
			count = 0
			words = append(words, newWord)
		}

		count++
		dict[newWord] = count
	}

	sort.SliceStable(words, func(i, j int) bool {
		return dict[words[i]] > dict[words[j]]
	})

	return
}

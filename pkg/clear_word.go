package pkg

import "strings"

func ClearWord(source string) string {
	source = strings.ToLower(source)
	source = strings.TrimSuffix(source, ":")
	source = strings.TrimSuffix(source, ";")
	source = strings.TrimSuffix(source, ".")
	source = strings.TrimSuffix(source, ",")
	source = strings.TrimSuffix(source, ":i.")
	source = strings.TrimSuffix(source, ":ii.")
	source = strings.TrimSuffix(source, ":iii.")
	source = strings.TrimSuffix(source, ":iv.")
	source = strings.TrimSuffix(source, ":v.")

	return source
}

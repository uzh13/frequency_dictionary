package internal

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
)

func OpenFile(file string) string {
	f, err := os.Open(file)
	if err != nil {
		fmt.Println("Unable to open file:", err)
		os.Exit(1)
	}
	defer f.Close()

	wr := bytes.Buffer{}
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		wr.WriteString(sc.Text())
	}

	return wr.String()
}

package internal

import (
	"fmt"
	"os"
)

func Save(file string, text string) {
	fileToWrite, err := os.Create(file)
	if err != nil {
		fmt.Println("Unable to create file:", err)
		os.Exit(1)
	}

	defer fileToWrite.Close()

	fileToWrite.WriteString(text)
}
